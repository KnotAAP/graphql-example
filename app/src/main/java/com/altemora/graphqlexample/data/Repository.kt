package com.altemora.graphqlexample.data

import android.annotation.SuppressLint
import android.content.Context
import com.altemora.graphqlexample.data.graphql.GraphqlApi
import com.altemora.graphqlexample.ui.dto.DataItem

class Repository {
    @SuppressLint("CheckResult")
    fun getItemsList(callback: OnItemsList) {
        GraphqlApi().getCountries(callback)
    }

    interface OnItemsList {
        fun onItemsSuccess(list: ArrayList<DataItem>)
        fun onItemsError(error: Throwable)
    }
}