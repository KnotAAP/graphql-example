package com.altemora.graphqlexample.data.graphql

import com.apollographql.apollo.ApolloClient
import okhttp3.OkHttpClient


object ApiProvider {
    private const val URL = "https://countries.trevorblades.com/"

    fun api(): ApolloClient {
        val okHttp = OkHttpClient
            .Builder()
            .addInterceptor { chain ->
                val original = chain.request()
                val builder = original.newBuilder().method(
                    original.method(),
                    original.body()
                )
                chain.proceed(builder.build())
            }
            .build()
        return ApolloClient.builder()
            .serverUrl(URL)
            .okHttpClient(okHttp)
            .build()
    }
}