package com.altemora.graphqlexample.data.graphql

import CountriesSmplQuery
import android.util.Log
import com.altemora.graphqlexample.data.Repository
import com.altemora.graphqlexample.ui.dto.DataItem
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException

class GraphqlApi {

    fun getCountries(callback: Repository.OnItemsList) {
        ApiProvider.api().query(
            CountriesSmplQuery()
        ).enqueue(object : ApolloCall.Callback<CountriesSmplQuery.Data>() {
            override fun onFailure(e: ApolloException) {
                Log.e("Apollo", "Error: " + e.message.toString())
                callback.onItemsError(e)
            }

            override fun onResponse(response: Response<CountriesSmplQuery.Data>) {
                Log.e("Apollo", "Response:" + response.data().toString())
                val list = ArrayList<DataItem>()
                for (country in response.data()?.countries!!) {
                    if (country != null) {
                        list.add(
                            DataItem(
                                country.name.toString(),
                                country.native_.toString(),
                                "",
                                country.code.toString(),
                                country.phone.toString(),
                                country.currency.toString()
                            )
                        )
                    }
                }
                callback.onItemsSuccess(list)
            }
        })
    }
}