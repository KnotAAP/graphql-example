package com.altemora.graphqlexample.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.altemora.graphqlexample.R
import com.altemora.graphqlexample.ui.dto.DataItem
import com.altemora.graphqlexample.ui.fragments.DetailsFragment
import com.altemora.graphqlexample.ui.fragments.ItemListFragment

class MainActivity : AppCompatActivity(R.layout.activity_main) {
    lateinit var itemList: ItemListFragment
    lateinit var details: DetailsFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        itemList = ItemListFragment.newInstance()
        details = DetailsFragment.newInstance()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.listFragment, itemList)
            .replace(R.id.detailsFragment, details)
            .commit()
        itemList.setListener(details.getListener())
    }

    interface FragmentDataListener {
        fun onSendDataItem(item: DataItem)
    }
}
