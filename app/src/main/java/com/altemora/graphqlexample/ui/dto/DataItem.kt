package com.altemora.graphqlexample.ui.dto

import android.util.Log
import android.webkit.WebView
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

data class DataItem(
    val name: String,
    val native: String,
    val flag: String?,
    val code: String,
    val phone: String,
    val currency: String
)

@BindingAdapter("url")
fun bindImage(view: ImageView, url: String) {
    if (!url.isEmpty())
        Picasso.get()
            .load(url)
            .fit()
            .into(view)
}

@BindingAdapter("emoji")
fun bindEmoji(view: WebView, emoji: String) {
    if (!emoji.isEmpty()) {
        //TODO: Bind emoji
    }
}