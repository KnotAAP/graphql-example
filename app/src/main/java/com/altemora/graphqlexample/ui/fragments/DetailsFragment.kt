package com.altemora.graphqlexample.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider

import com.altemora.graphqlexample.R
import com.altemora.graphqlexample.databinding.FragmentDetailsBinding
import com.altemora.graphqlexample.ui.activities.MainActivity
import com.altemora.graphqlexample.ui.dto.DataItem

class DetailsFragment : Fragment(), MainActivity.FragmentDataListener {
    private lateinit var binding: FragmentDetailsBinding;

    companion object {
        fun newInstance() = DetailsFragment()
    }

    private lateinit var viewModel: DetailsFragmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false)
        binding.item = DataItem("", "", "", "", "", "")
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(DetailsFragmentViewModel::class.java)
    }

    fun getListener(): MainActivity.FragmentDataListener {
        return this;
    }

    override fun onSendDataItem(item: DataItem) {
        binding.item = item;
    }

}
