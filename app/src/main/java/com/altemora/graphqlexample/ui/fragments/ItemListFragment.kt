package com.altemora.graphqlexample.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.altemora.graphqlexample.R
import com.altemora.graphqlexample.data.Repository
import com.altemora.graphqlexample.ui.activities.MainActivity
import com.altemora.graphqlexample.ui.dto.DataItem


class ItemListFragment : Fragment(), Repository.OnItemsList {
    private lateinit var listener: MainActivity.FragmentDataListener;

    companion object {
        fun newInstance() = ItemListFragment()
    }

    private lateinit var viewModel: ItemListViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_item_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ItemListViewModel::class.java)
        viewModel.createAdapter(context!!, view!!)
        viewModel.getDataItems(this)
        viewModel.setFragmentDataListener(listener)
    }

    fun setListener(listener: MainActivity.FragmentDataListener) {
        this.listener = listener
    }

    override fun onItemsSuccess(list: ArrayList<DataItem>) {
        activity?.runOnUiThread {
            viewModel.onItemsSuccess(list)
        }
    }

    override fun onItemsError(error: Throwable) {
        activity?.runOnUiThread {
            Toast.makeText(context, error.message, Toast.LENGTH_LONG).show()
            viewModel.onItemsError(error)
        }
    }
}
