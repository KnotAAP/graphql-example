package com.altemora.graphqlexample.ui.fragments

import android.content.Context
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.altemora.graphqlexample.R
import com.altemora.graphqlexample.ui.dto.DataItem
import com.altemora.graphqlexample.data.Repository
import com.altemora.graphqlexample.ui.activities.MainActivity
import com.altemora.graphqlexample.ui.adapters.ItemsAdapter

class ItemListViewModel : ViewModel(), ItemsAdapter.Callback {
    private val repository = Repository()
    private lateinit var adapter: ItemsAdapter;
    private lateinit var fragmentListener: MainActivity.FragmentDataListener;

    fun getDataItems(callback: Repository.OnItemsList) {
        repository.getItemsList(callback)
    }

    fun setFragmentDataListener(listener: MainActivity.FragmentDataListener) {
        fragmentListener = listener
    }

    fun createAdapter(context: Context, view: View) {
        adapter = ItemsAdapter(ArrayList(), this)
        val recycler = view.findViewById<RecyclerView>(R.id.list_dataitems)
        val decorator = DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
        decorator.setDrawable(context.getDrawable(R.drawable.divider)!!)
        recycler?.addItemDecoration(decorator)
        recycler?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recycler?.adapter = adapter
    }

    fun onItemsSuccess(list: ArrayList<DataItem>) {
        adapter.items = list
        adapter.notifyDataSetChanged()
    }

    fun onItemsError(error: Throwable) {
        //TODO:something
    }

    override fun onItemClicked(context: Context, item: DataItem) {
        fragmentListener.onSendDataItem(item)
    }
}
